# CHANGELOG



## v0.2.1 (2024-04-03)

### Fix

* fix(deploy): add deploy pipeline ([`b43d277`](https://gitlab.com/mbaistrocchi/my-package/-/commit/b43d277241789f2e93fa393a5b85b13c2fb49651))


## v0.2.0 (2024-04-03)

### Feature

* feat(addition): add addition feature ([`5f56370`](https://gitlab.com/mbaistrocchi/my-package/-/commit/5f56370541062a33d203370d17932de73a03d7a5))


## v0.1.0 (2024-04-03)

### Feature

* feat(sr): add semantic release cd pipeline ([`5f6233a`](https://gitlab.com/mbaistrocchi/my-package/-/commit/5f6233ad571a13214e14dec67f8dfc29908af75a))

### Unknown

* add semantic_release config ([`c082c70`](https://gitlab.com/mbaistrocchi/my-package/-/commit/c082c7005091c48005db6eb29ffbd5f1ef5d0227))

* add python-semantic-release dev dependency ([`6867eb9`](https://gitlab.com/mbaistrocchi/my-package/-/commit/6867eb9b7cfdd35a919bb47db8160a8375ceb09e))

* Add pyproject.toml to init poetry ([`1ed647b`](https://gitlab.com/mbaistrocchi/my-package/-/commit/1ed647b7bd0525d5e7b1a05d846d7a2d0933c4b5))

* Update .gitlab-ci.yml file ([`dbbf9b0`](https://gitlab.com/mbaistrocchi/my-package/-/commit/dbbf9b04b322a8a61374e460d2520e429b44408e))

* primer paso: stages ([`48be49a`](https://gitlab.com/mbaistrocchi/my-package/-/commit/48be49a30d6af84c7a475db73c62668b8ce8905d))

* Initial commit ([`8c81ceb`](https://gitlab.com/mbaistrocchi/my-package/-/commit/8c81ceb1862f9a739afc6e2deb0f03720eb38272))
